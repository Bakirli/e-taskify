package etaskify.controllers;

import etaskify.forms.Task;
import etaskify.repos.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    TaskRepository repository;

    @GetMapping("/")
    public List<Task> getTasks() {
        return repository.findAll();
    }

    @PostMapping("/")
    public Task createTask(@RequestBody Task task) {
        repository.save(task);
        return task;
    }

    @PutMapping("/{id}")
    public Task saveUser(@PathVariable(name = "id") long id,
                         @RequestBody Task task) {
        Optional<Task> optionalTask = repository.findById(id);

        if (optionalTask.isPresent()) {
            Task taskOpt = optionalTask.get();
            taskOpt.setStatus(task.getStatus());
        } else {
            return repository.save(task);
        }

        return task;
    }
}
