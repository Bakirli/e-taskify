package etaskify.controllers;

import etaskify.forms.User;
import etaskify.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/manageuser")
public class ManageUserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/")
    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll()
                .forEach(user -> users.add(user));
        return users;
    }

    @ResponseStatus( HttpStatus.CREATED )
    @PostMapping("/")
    public User addUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping("/{id}")
    public User saveUser(@PathVariable(name = "id") long id,
                         @RequestBody User user) {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isPresent()) {
            User userOpt = optionalUser.get();
            userOpt.setName(user.getName());
            userOpt.setSurname(user.getSurname());
            userOpt.setEmail(user.getEmail());
            userOpt.setEmail(user.getEmail());
        } else {
            return userRepository.save(user);
        }

        return user;
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable(name = "id") long id) {
        userRepository.deleteById(id);
    }

}
