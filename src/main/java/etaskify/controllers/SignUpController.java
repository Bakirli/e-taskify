package etaskify.controllers;

import etaskify.forms.Organization;
import etaskify.repos.OrgRepository;
import etaskify.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/signup")
public class SignUpController {

    @Autowired
    OrgRepository repository;

    @GetMapping("/")
    public ModelAndView signup() {
        ModelAndView mav = new ModelAndView("signup");
        Organization org = new Organization();
//        User user = new User();
        mav.addObject("org", org);
//        mav.addObject("user", user);
        return mav;
    }

    @PostMapping("/")
    public Organization createOrganization(@ModelAttribute(name = "org") Organization org) {

        Organization organization = new Organization();
        organization.setName(org.getName());
        organization.setAddress(org.getAddress());
        organization.setPhone(org.getPhone());

        organization.setUsername(org.getUsername());
        organization.setEmail(org.getEmail());
        organization.setPassword(org.getPassword());

        repository.save(organization);
        return organization;
    }
}
