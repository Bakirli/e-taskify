package etaskify.repos;

import etaskify.forms.Organization;
import org.springframework.data.repository.CrudRepository;

public interface OrgRepository extends CrudRepository<Organization, Long> {
}
